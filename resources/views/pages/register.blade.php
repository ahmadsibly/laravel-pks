@extends('layouts.master')
@section('title')
    Register
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
    <p><b>Sign Up Form</b></p>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name :</label><br>
        <input type="text" name="firstname" id="firstname" required /><br><br>
        <label for="lastname">Last name :</label><br>
        <input type="text" name="lastname" id="lastname" required /><br><br>
        <label for="gender">Gender :</label><br>
        <input type="radio" name="gender" id="gender" value="Male" />Male<br>
        <input type="radio" name="gender" id="gender" value="Female" />Female<br><br>
        <label for="natioanaly">Nationality :</label><br>
        <select name="natioanaly" id="natioanaly" required>
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
        </select><br><br>
        <label for="language">Language Spoken :</label><br>
        <input type="checkbox" name="language" id="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language" id="language" value="English">English<br>
        <input type="checkbox" name="language" id="language" value="Other">Other<br><br>
        <label for="bio">Bio :</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10" required></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
@endsection
