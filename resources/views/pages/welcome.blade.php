@extends('layouts.master')
@section('title')
    Welcome
@endsection

@section('content')
    <h1>SELAMAT DATANG ! {{ $firstname }}</h1>
    <h2>Terima kasih telah bergabung di website kami. Media Belajar kita bersama!</h2>
    <h3>Berikut ini adalah biodata anda</h3>
    <ul>
        <li>Nama : {{ $firstname }} {{ $lastname }}</li>
        <li>Gender : {{ $gender }}</li>
        <li>Natioanaly : {{ $natioanaly }}</li>
        <li>Language : {{ $language }}</li>
        <li>Bio : {{ $bio }}</li>
    </ul>
@endsection
