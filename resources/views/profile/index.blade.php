@extends('layouts.master')
@section('title')
    Profile
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush
{{-- 
@push('js')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "columnDefs": [{
                    "width": "3%",
                    "targets": 0
                }]
            });
        });
    </script>
@endpush --}}
@section('content')
    <!-- Profile Image -->

    <div class="card-body box-profile">
        {{-- <div class="text-center">
            <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg"
                alt="User profile picture">
        </div> --}}

        <h3 class="profile-username text-center">{{ $user->name }}</h3>

        {{-- <p class="text-muted text-center">Software Engineer</p> --}}

        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Email</b> <a class="float-right">{{ $user->email }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Umur</b> <a class="float-right">{{ $user->profile->umur }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Alamat</b> <a class="float-right">{{ $user->profile->alamat }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Bio</b> <a class="float-right">{{ $user->profile->bio }}</a>
                    </li>
                </ul>
            </div>
        </div>

        {{-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
    </div>
    <!-- /.card-body -->
@endsection
