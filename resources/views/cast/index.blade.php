@extends('layouts.master')
@section('title')
    Data Cast
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('js')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "columnDefs": [{
                    "width": "3%",
                    "targets": 0
                }]
            });
        });
    </script>
@endpush
@section('content')
    <div class="mb-3">
        <a href="{{ route('cast.create') }}" class="btn btn-primary">Add Data</a>
    </div>

    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($cast as $value)
                <tr>
                    <td class="width">{{ $loop->iteration }}</td>
                    <td>{{ $value->nama }}</td>
                    <td>{{ $value->umur }}</td>
                    <td>{{ $value->bio }}</td>
                    <td>
                        <a href="/cast/{{ $value->id }}" class="btn btn-success">Show</a>
                        <a href="/cast/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                        <form action="/cast/{{ $value->id }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"
                                onclick="return confirm('Apakah anda yakin akan menghapus data ini?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
@endsection
