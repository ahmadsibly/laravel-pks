@extends('layouts.master')
@section('title')
    Show Cast {{ $cast->id }}
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('js')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "columnDefs": [{
                    "width": "3%",
                    "targets": 0
                }]
            });
        });
    </script>
@endpush
@section('content')
    <h4>{{ $cast->nama }}</h4>
    <p>{{ $cast->umur }}</p>
    <p>{{ $cast->bio }}</p>
@endsection
