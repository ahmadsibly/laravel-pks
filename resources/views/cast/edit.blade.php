@extends('layouts.master')
@section('title')
    Edit Cast {{ $cast->id }}
@endsection
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@push('js')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "columnDefs": [{
                    "width": "3%",
                    "targets": 0
                }]
            });
        });
    </script>
@endpush
@section('content')
    <div>
        <form action="/cast/{{ $cast->id }}" method="POST">
            @csrf
            {{-- ganti method jadi PUT --}}
            @method('PUT')
            <div class="form-group">
                <label for="nama">Title</label>
                <input type="text" class="form-control" name="nama" value="{{ $cast->nama }}" id="title"
                    placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="text" class="form-control" name="umur" value="{{ $cast->umur }}" id="umur"
                    placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" value="{{ $cast->bio }}" id="bio"
                    placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection
